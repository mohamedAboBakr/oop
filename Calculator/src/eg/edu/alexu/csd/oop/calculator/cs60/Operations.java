package eg.edu.alexu.csd.oop.calculator.cs60;

import java.util.Stack;

public class Operations {
	  
	  private Character oBracket = new Character('(');
	  private Character cBracket = new Character(')');
	  private Character add_sign = new Character('+');
	  private Character sub_sign = new Character('-');
	  private Character mul_sign = new Character('*');
	  private Character div_sign = new Character('/');
	  private Character dot_sign = new Character('.');
	  private Double Zero = new Double(0);
	  
      public String Eval(String input){
    	  
    	   input = input.replaceAll("\\s+","");
    	   Stack <String> stk = new Stack<>();
    	   int length = input.length();
    	   int open = 0 , close = 0 ;
    	   int colsed_flag = 0 ;
    	   if(length == 0) return new String("Error ...Emty Input..");
    	   for(int i=0 ; i< length ; i++){
    		      char c = input.charAt(i) ;
    		     
    		      // open bracket (
    		      if(oBracket.equals(c)) {
    		    	  if(colsed_flag == 1) return new String("Invalid Expression");
    		    	   stk.push(new Character(input.charAt(i)).toString());
    		    	   open++ ;
    		    	   colsed_flag = 0 ;
    		      }
    		      
    		      // close bracket )
    		      else if(cBracket.equals(c)) {
    		    	  if(open == 0){
    		    		   return new String("Invalid Expression");
    		    	  }
    		    	  
    		    	  if(Is_operationSign(stk.peek())){
    		    		  return new String("Invalid Expression");
    		    	  }
    		    	  
    		    	  String value = stk.pop();
    		    	  while(!stk.empty()){
    		    		   if(stk.peek().equals("(")) {stk.pop(); open--; break;}
    		    		   String sign = stk.pop();
    		    		   String nxt_num = stk.pop();
    		    		   value = make_operation(value , sign , nxt_num);
    		    		   if(value.equals("Error .. Dividing by Zero.")) return value ;
    		    	  }
    		    	  colsed_flag = 1 ;
    		    	  // what happen after closing
    		    	  if(stk.empty())stk.push(value);
    		    	  else{
    		    		  String top = stk.peek();
    		    		  if(top.equals("*") || top.equals("/")){
    		    			   String operation = stk.pop();
    		    			   String nex_num = stk.pop();
    		    			   String v  = make_operation(value , operation , nex_num);
    		    			   stk.push(v);
    		    		  } else stk.push(value);
    		    	  }
    		      }
    		      
    		      // operation sign + - * /
    		      else if(Is_operationSign(String.valueOf(c))){
    		    	   
    		    	    if(stk.empty())  return new String("Invalid Expression"); 
    		    	    String value = stk.peek();
    		    	    if(value.equals("(")) return new  String ("Invalid Expression");
    		    	    stk.push(String.valueOf(c));
    		    	    colsed_flag = 0 ;
    		      }
    		      
    		      // number 
    		      else{
    		    	  
    		    	  String value ="";
    		    	  for( ; i<length&& Is_digit(input.charAt(i)) ; i++){
    		    		    value += String.valueOf(input.charAt(i));
    		    	  }
    		    	  i-- ;
    		    	  
    		    	  if(!Is_valid(value) ||  colsed_flag == 1) return new  String ("Invalid Expression 1") ;
    		    	  if(stk.empty()) stk.push(value);
    		    	  else{
    		    		  String sign = stk.peek();
    		    		  if(sign.equals("+") || sign.equals("-") || sign.equals("(")) stk.push(value) ;
    		    		  else{
    		    			  String operation = stk.pop();
    		    			  String v2 = stk.pop();
    		    			  String res = make_operation(value , operation , v2);
    		    			  if(res.equals("Error .. Dividing by Zero.")) return res ;
    		    			  stk.push(res);
    		    		  }
    		    		  colsed_flag = 0 ;
    		    	  }
    		    	  
    		      }
    		      
    	   }
    	   
    	   if(open != 0) return new String("Invalid Expression");
    	   String top = stk.pop();
    	   if(!Is_valid(top)) return new String("Invalid Expression");
    	   if(stk.empty() || stk.peek().equals("(")) return top ;
    	   while( !stk.empty() && !stk.peek().equals("(")){
    		    String op = stk.pop();
    		    String v2 = stk.pop();
    		    top = make_operation(top , op , v2);
    	   }
    	   return top ;
      }
      
      
      private boolean Is_digit(char c){
    	      if(c=='(' || c == ')' || c == '+' || c=='-'|| c== '*' || c=='/') return false ;
    	      return true ;
      }
      
      
      private String make_operation (String v1 , String s , String v2){
    	    switch(s){
    	    case "+": return Add(v1,v2);
    	    case "-": return Subtract(v2,v1);
    	    case "*": return Multiply(v1,v2);
    	    case "/": return Divide(v2,v1);
    	    default : return "Error";
    	    }
      }
      
      private boolean Is_operationSign(String c){
    	     return  c.equals("+") || c.equals("/")  || c.equals("*") || c.equals("-") ;
      }
      
      private boolean Is_valid(String s){
    	  if(s.matches("[+-]?\\d+(?:\\.\\d+)?")) return true ;
    	  return false ;
      }
      

      private String Add(String f1 , String f2){
    	   Double fl1 = new Double(f1) ;
    	   Double fl2 = new Double(f2) ;
    	   Double res = new Double(fl1.doubleValue() + fl2.doubleValue());
    	   return res.toString() ;
      }
      
      private String Subtract(String f1 , String f2){
    	  Double fl1 = new Double(f1) ;
    	  Double fl2 = new Double(f2) ;
    	  Double res = new Double(fl1.doubleValue() - fl2.doubleValue());
   	      return res.toString() ;
     }
      
      private String Multiply(String f1 , String f2){
    	  Double fl1 = new Double(f1) ;
    	  Double fl2 = new Double(f2) ;
    	  Double res = new Double(fl1.doubleValue() * fl2.doubleValue());
   	      return res.toString() ;
      }
      
      private String Divide(String f1 , String f2){
    	  Double fl1 = new Double(f1) ;
    	  Double fl2 = new Double(f2) ;
   	      if(fl2.equals(Zero)) return "Error .. Dividing by Zero.";
   	      Double res = new Double(fl1.doubleValue() / fl2.doubleValue());
   	      return res.toString() ;
        }
      
      public void test(String s){
    	   System.out.println(Eval(s));
      }
}
