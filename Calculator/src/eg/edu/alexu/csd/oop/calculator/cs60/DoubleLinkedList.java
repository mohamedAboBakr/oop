package eg.edu.alexu.csd.oop.calculator.cs60;

import java.util.List;

class Node{
	 private String Expression  ;
	 private Node next , previous ;
	 public Node(String e){
		 Expression = e ;
		 next = null ;
		 previous = null ;
	 }
	 
	 public void setNext(Node next){
		 this.next = next ;
	 }
	 
	 public void setPrev(Node previous){
		 this.previous = previous ;
	 }
	 
	 public Node getNext(){ return next ;}
	 public Node getPrev(){return previous;}
	 
	 public String getExp(){
		  return Expression ;
	 }
	 
}


public class DoubleLinkedList {
      private Node head , tail , current ;
      private int size ;
       
      public DoubleLinkedList(){
    	   size = 0 ;
    	   head = null ;
    	   tail = null ;
    	   current = null ;
      }
      
      public void Add(String exp){
    	      Node new_node = new Node(exp);
    	      if(size == 0 ){
    	    	   head = new_node ;
    	    	   tail = new_node ;
    	    	   current = new_node ;
    	    	   size = 1 ;
    	    	   return ;
    	      }
    	      
    	      if(size == 5){
    	    	   tail.setNext(new_node);
    	    	   new_node.setPrev(tail);
    	    	   head = head.getNext();
    	    	   head.setPrev(null);
    	    	   tail = new_node;
    	    	   current = tail;
    	    	   return ;
    	      }
    	      
    	       tail.setNext(new_node);
	    	   new_node.setPrev(tail);
	    	   tail = new_node;
	    	   current = tail;
	    	   size++;
      }
      
      public Node cuur_node (){ return current ;}
      
      public String Curr(){
    	  if(current == null) return null ;
    	  return current.getExp();
      }
      public String prev(){
    	    if(current == null ) return null ;
    	    if(current.equals(head)) return null ;
    	    current = current.getPrev() ;
    	    return current.getExp() ;
      }
      
      public String next(){
    	if(current == null ) return null ;  
  	    if(current.equals(tail)) return null ;
  	    current = current.getNext() ;
  	    return current.getExp() ;
      }
      
      public void check(){
    	  Node n = current ;
    	  while(n != null){
    		   System.out.println(n.getExp());
    		   n = n.getPrev() ;
    	  }
      }
      
      public String[] save(){
    	  
    	  String ans[] = new String[size] ;
    	  Node curr = head ;
    	  int index = 0 ;
    	  while(curr != null){
    		  String s = "" ;
    		  s += curr.getExp();
    		  if(curr.equals(current)) s += "  &";
    		  curr = curr.getNext();
    		  ans[index++] = s ;
    	  }

    	  return ans ;
      }
      
      public void load (List<String> s){
    	   size = 0 ;
    	   head = null ; current = null ; tail = null ;
    	   
    	   int curr_index=0 , i=0;
    	   for(String str : s){
    		     if(str == null) break ;
    		     if(str.charAt(str.length()-1) == '&') curr_index = i ;
    		     Add(str.split(" ")[0]);
    		     i++ ;
    	   }
    	   
    	   Node n = head ;
    	   i=0;
    	   while(i<curr_index){
    		     n = n.getNext() ; i++ ;
    	   }
    	   current = n ;
     }
}
