package eg.edu.alexu.csd.oop.calculator.cs60;

import java.util.List;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import eg.edu.alexu.csd.oop.calculator.Calculator;

public class CalculatorUtil implements Calculator {
    
	private String Expression; 
	private Operations operation_obj ;
	private String File_name ;
	private File WR_file ;
	private DoubleLinkedList dbLS ;
	
	
	public CalculatorUtil(){
		File_name = "Calc.txt" ;
		WR_file   = new File(File_name);
		dbLS = new DoubleLinkedList();
		operation_obj = new Operations();
	}
	
   public void checck(){
		 dbLS.check();
	}
	@Override
	public void input(String s) {
		 dbLS.Add(s);
	}

	@Override
	public String getResult() throws RuntimeException {
		String curr = dbLS.Curr() ;
		if(curr == null) throw new RuntimeException("Error..Invalid query\n");
		String ans = operation_obj.Eval(curr);
		if(ans.equals("Error ...Emty Input.."))  throw new RuntimeException("Error ...Emty Input..");
		if(ans.equals("Invalid Expression"))  throw new RuntimeException("Invalid Expression");
		if(ans.equals("Error .. Dividing by Zero."))  throw new RuntimeException("Error .. Dividing by Zero.");
		return ans;
	}

	
	@Override
	public String current() {
		return dbLS.Curr() ;
	}

	@Override
	public String prev() {
		return dbLS.prev() ;
	}

	@Override
	public String next() {
		return dbLS.next() ;
	}

	
	@Override
	public void save() {
	     // if(dbLS.cuur_node() == null ) throw new RuntimeException("There Is nothing tO Save");
		 String toFile[] = dbLS.save() ;
		 try {
	            FileWriter fileWriter = new FileWriter(File_name);
	            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
	            int sz = toFile.length ;
	            for(String s : toFile){
	            	 if(s == null) break ;
	            	 bufferedWriter.write(s);
	            	 bufferedWriter.newLine();
	            }
	            bufferedWriter.close();
	        }
	        catch(IOException ex) {
	                 System.out.println(
	                "Error writing to file '"
	                + File_name + "'");
	        }
	}

	
	@Override
	public void load() {
		List<String> FromFilel = new ArrayList<>();
		String Res = "" ;
		String line = null;
		int flag = 0 ;
		try {
	            FileReader fileReader = new FileReader(File_name);
	            BufferedReader bufferedReader =  new BufferedReader(fileReader);

	            while((line = bufferedReader.readLine()) != null) {
	            	FromFilel.add(line);
	            	flag=1 ;
	            }   
	            
	           // if(flag==0) throw new RuntimeException("Empty File");
	            dbLS.load(FromFilel);
	            bufferedReader.close();         
	        }
	    catch(FileNotFoundException ex) {
	            System.out.println("Unable to open file '" + File_name + "'");                
	        }
	        catch(IOException ex) {
	            System.out.println("Error reading file '" + File_name + "'");                  
	        }
	}

}
