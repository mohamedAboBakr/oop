package eg.edu.alexu.csd.oop.calculator.cs60;

import java.awt.*;
import java.awt.event.*;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class GUI extends JFrame {
   private Button[] btnNumbers; 
   private Button btnHash, btnStar;
   private TextField tfDisplay;
   private CalculatorUtil calc_obj ;
   private String output ;
   private int flag = 0 ;
   public GUI () {
	  setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	  calc_obj = new CalculatorUtil();
      Panel panelDisplay = new Panel(new FlowLayout());
      output = "" ;
      tfDisplay = new TextField(output, 50);
      tfDisplay.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent e) {
                 output = tfDisplay.getText() ;
            }
       });
      panelDisplay.add(tfDisplay);
      
      
      Panel panelButtons = new Panel(new GridLayout(6, 4));
      btnNumbers = new Button[21]; 
      
      btnNumbers[16] = new Button("S");  
      btnNumbers[16].addActionListener(new ActionListener(){
    	  @Override
  		public void actionPerformed(ActionEvent arg0) {
    		  calc_obj.save();
    		  flag = 0 ;
  		}
      });
      panelButtons.add(btnNumbers[16]); 
      
      
      btnNumbers[17] = new Button("L");  
      btnNumbers[17].addActionListener(new ActionListener(){
    	  @Override
  		public void actionPerformed(ActionEvent arg0) {
    		  calc_obj.load();
    		  String curr = calc_obj.current() ;
    		  if(curr == null ) curr = "" ;
    		  output = curr ;
    		  tfDisplay.setText(output);
    		  flag = 0 ;
  		}
      });
      panelButtons.add(btnNumbers[17]); 
      
      
      btnNumbers[18] = new Button("P");  
      btnNumbers[18].addActionListener(new ActionListener(){
    	  @Override
  		public void actionPerformed(ActionEvent arg0) {
  			  String prev = calc_obj.prev();
  			  if(prev != null){
  				   output = prev ;
  				   tfDisplay.setText(output);
  				   flag = 0 ;
  			  }
  		}
      });
      panelButtons.add(btnNumbers[18]); 
      
      
      btnNumbers[19] = new Button("N");  
      btnNumbers[19].addActionListener(new ActionListener(){
    	  @Override
  		public void actionPerformed(ActionEvent arg0) {
    		  String nxt = calc_obj.next();
  			  if(nxt != null){
  				   output = nxt ;
  				   tfDisplay.setText(output);
  				   flag = 0 ;
  			  }
  		}
      });
      panelButtons.add(btnNumbers[19]); 
      
      
      btnNumbers[1] = new Button("1");  
      btnNumbers[1].addActionListener(new ActionListener(){
    	  @Override
  		public void actionPerformed(ActionEvent arg0) {
    		  if(flag == 1){
     			 flag = 0 ;
     			 output = "" ;
     		 }
  			 output += "1" ;
  			 tfDisplay.setText(output);
  		}
      });
      panelButtons.add(btnNumbers[1]);  
      
      
      btnNumbers[2] = new Button("2");
      btnNumbers[2].addActionListener(new ActionListener(){
    	  @Override
  		public void actionPerformed(ActionEvent arg0) {
    		  if(flag == 1){
     			 flag = 0 ;
     			 output = "" ;
     		 }
    		  output += "2" ;
   			  tfDisplay.setText(output);
  		}
      });
      panelButtons.add(btnNumbers[2]);
      
      
      btnNumbers[3] = new Button("3");
      btnNumbers[3].addActionListener(new ActionListener(){
    	  @Override
  		public void actionPerformed(ActionEvent arg0) {
    		  if(flag == 1){
     			 flag = 0 ;
     			 output = "" ;
     		 }
    		 output += "3" ;
   			 tfDisplay.setText(output);
  		}
      });
      panelButtons.add(btnNumbers[3]);
      
      
      
      btnNumbers[4] = new Button("4");
      btnNumbers[4].addActionListener(new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			 if(flag == 1){
    			 flag = 0 ;
    			 output = "" ;
    		 }
			 output += "4" ;
 			 tfDisplay.setText(output);  
		}
	}) ;
      panelButtons.add(btnNumbers[4]);
      
      
      btnNumbers[5] = new Button("5");
      btnNumbers[5].addActionListener(new ActionListener(){
    	  @Override
  		public void actionPerformed(ActionEvent arg0) {
    		  if(flag == 1){
     			 flag = 0 ;
     			 output = "" ;
     		 }
    		 output += "5" ;
   			 tfDisplay.setText(output);
  		}
      });
      panelButtons.add(btnNumbers[5]);
      
      
      btnNumbers[6] = new Button("6");
      btnNumbers[6].addActionListener(new ActionListener(){
    	  @Override
  		public void actionPerformed(ActionEvent arg0) {
    		  if(flag == 1){
     			 flag = 0 ;
     			 output = "" ;
     		 }
    		     output += "6" ;
    			 tfDisplay.setText(output);
  		}
      });
      panelButtons.add(btnNumbers[6]);
      
      
      btnNumbers[7] = new Button("7");
      btnNumbers[7].addActionListener(new ActionListener(){
    	  @Override
  		public void actionPerformed(ActionEvent arg0) {
    		  if(flag == 1){
     			 flag = 0 ;
     			 output = "" ;
     		 }
    		     output += "7" ;
    			 tfDisplay.setText(output);
  		}
      });
      panelButtons.add(btnNumbers[7]);
      
      
      btnNumbers[8] = new Button("8");
      btnNumbers[8].addActionListener(new ActionListener(){
    	  @Override
  		public void actionPerformed(ActionEvent arg0) {
    		  if(flag == 1){
     			 flag = 0 ;
     			 output = "" ;
     		 }
    		     output += "8" ;
    			 tfDisplay.setText(output);
  		}
      });
      panelButtons.add(btnNumbers[8]);
      
      
      btnNumbers[9] = new Button("9");
      btnNumbers[9].addActionListener(new ActionListener(){
    	  @Override
  		public void actionPerformed(ActionEvent arg0) {
    		  if(flag == 1){
     			 flag = 0 ;
     			 output = "" ;
     		 }
    		      output += "9" ;
    			 tfDisplay.setText(output);
  		}
      });
      panelButtons.add(btnNumbers[9]);
      
      
      btnNumbers[0] = new Button("0");
      btnNumbers[0].addActionListener(new ActionListener(){
    	  @Override
  		public void actionPerformed(ActionEvent arg0) {
    		   if(flag == 1){
     			 flag = 0 ;
     			 output = "" ;
     		   }
    		     output += "0" ;
    			 tfDisplay.setText(output);
  		}
      });
      panelButtons.add(btnNumbers[0]);
      
      
      btnNumbers[10] = new Button("+");
      btnNumbers[10].addActionListener(new ActionListener(){
    	  @Override
  		public void actionPerformed(ActionEvent arg0) {
    		 flag=0;  
    		 output += "+" ;
 			 tfDisplay.setText(output);
  		}
      });
      panelButtons.add(btnNumbers[10]);
  
      
      btnNumbers[11] = new Button("-");
      btnNumbers[11].addActionListener(new ActionListener(){
    	  @Override
  		public void actionPerformed(ActionEvent arg0) {
    		  flag=0;
    		  output += "-" ;
 			  tfDisplay.setText(output);
  		}
      });
      panelButtons.add(btnNumbers[11]);
      
      
      btnNumbers[12] = new Button("*");
      btnNumbers[12].addActionListener(new ActionListener(){
    	  @Override
  		public void actionPerformed(ActionEvent arg0) {
    		  flag=0;
    		  output += "*" ;
 			 tfDisplay.setText(output);
  		}
      });
      panelButtons.add(btnNumbers[12]);
      
      
      btnNumbers[13] = new Button("/");
      btnNumbers[13].addActionListener(new ActionListener(){
    	  @Override
  		public void actionPerformed(ActionEvent arg0) {
    		 flag=0;
    		 output += "/" ;
 			 tfDisplay.setText(output);
  		}
      });
      panelButtons.add(btnNumbers[13]);
      
      btnNumbers[14] = new Button("(");
      btnNumbers[14].addActionListener(new ActionListener(){
    	  @Override
  		public void actionPerformed(ActionEvent arg0) {
    		 if(flag == 1){
    			 flag = 0 ;
    			 output = "" ;
    		 }
    		 output += "(" ;
 			 tfDisplay.setText(output);
  		}
      });
      panelButtons.add(btnNumbers[14]);
      
      
      btnNumbers[15] = new Button(")");
      btnNumbers[15].addActionListener(new ActionListener(){
    	  @Override
  		public void actionPerformed(ActionEvent arg0) {
    		  if(flag == 1){
     			 flag = 0 ;
     			 output = "" ;
     		 }
    		 output += ")" ;
 			 tfDisplay.setText(output);
  		}
      });
      panelButtons.add(btnNumbers[15]);
      
      
      
      btnNumbers[20] = new Button("=");
      btnNumbers[20].addActionListener(new ActionListener(){
    	  @Override
  		public void actionPerformed(ActionEvent arg0) {
    		      if(flag == 1) return ;
    		      output = tfDisplay.getText();
  			      String curr = calc_obj.current() ;
  			      if(curr == null){
  			    	  if(!output.equals("")){
  			    	    try{   
  			    		     calc_obj.input(output);
  			    		     output = calc_obj.getResult() ;
  			    		     tfDisplay.setText(output);
  			    		     flag = 1 ;
  			    	    }catch(RuntimeException e){
  			    	    	JOptionPane.showMessageDialog(null, e.getMessage());
  			    	    }
  			    	  }
  			      }else {
  			    	  if(!output.equals("")){
  			    		    if(!curr.equals(output)){
  			    		    	try{   
  		  			    		     calc_obj.input(output);
  		  			    		     output = calc_obj.getResult() ;
  		  			    		     tfDisplay.setText(output);
  		  			    		     flag = 1 ;
  		  			    	    }catch(RuntimeException e){
  		  			    	    	JOptionPane.showMessageDialog(null, e.getMessage());
  		  			    	    }
  			    		    }
  			    	  }
  			      }
  		}
      });
      panelButtons.add(btnNumbers[20]);

 
      setLayout(new BorderLayout()); 
      add(panelDisplay, BorderLayout.NORTH);
      add(panelButtons, BorderLayout.CENTER);
 
      setTitle("BorderLayout Demo"); 
      setSize(500, 250);            
      setVisible(true);             
   }
 
 
   public static void main(String[] args) {
      new GUI(); 
   }
}
